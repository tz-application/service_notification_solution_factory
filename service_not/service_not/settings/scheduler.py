from celery.schedules import crontab
from kombu import Queue, Exchange

# очередь по уолчанию
CELERY_TASK_DEFAULT_QUEUE = "default"

# Список очередей
CELERY_QUEUES = (
    Queue("high", Exchange("high"), routing_key="high"),
    Queue("default", Exchange("default"), routing_key="default"),
    Queue("low", Exchange("low"), routing_key="low"),
)

# https://code.tutsplus.com/ru/tutorials/using-celery-with-django-for-background-task-processing--cms-28732
# https://docs.celeryproject.org/en/latest/userguide/periodic-tasks.html#crontab-schedules
# https://webdevblog.ru/python-celery/
# https://coderoad.ru/45452716/celery-beat-schedule-%D1%81-%D0%BA%D0%BE%D0%BD%D1%84%D0%B8%D0%B3%D1%83%D1%80%D0%B0%D1%86%D0%B8%D0%B5%D0%B9-timezone-%D0%BD%D0%B5-%D0%B7%D0%B0%D0%BF%D1%83%D1%81%D0%BA%D0%B0%D0%B5%D1%82%D1%81%D1%8F-%D0%B2-%D0%BD%D1%83%D0%B6%D0%BD%D0%BE%D0%B5-%D0%B2%D1%80%D0%B5%D0%BC%D1%8F
# Расписание выполнения задач
CELERY_BEAT_SCHEDULE = {
    "test": {
        "task": "app.tasks.check_work_run",
        "schedule": crontab(minute="*/1"),
    },  # выключение скидок

}

# запуск задач под их уровнем
CELERY_TASK_ROUTES = {
    'app.tasks.check_work_run': {'queue': 'default', },
    'app.tasks.get_mailing': {'queue': 'low', },
    'app.tasks.send_message': {'queue': 'low', },
}
