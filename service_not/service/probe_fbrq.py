import requests as req
from pydantic import BaseModel


class ApiResponse(BaseModel):
    id: int = 0
    text: str
    phone: int

    def set_id(self, id: int) -> None:
        self.id = id


class Msg(BaseModel):
    code: int
    message: str


class NotFoundApi(Exception):
    def __init__(self, response_status, response_text):
        super().__init__(response_status)
        self.response_text = response_text


class BadRequestApi(Exception):
    def __init__(self, response_status, response_text):
        super().__init__(response_status)
        self.response_text = response_text


class ProbeFbrq:
    def __init__(self, token: str) -> None:
        self.token = token
        self.link = 'https://probe.fbrq.cloud/v1'
        self.headers = {
            "Authorization": f"Bearer {self.token}"
        }

    def send_message(
        self,
        msg_id: int,
        send_request: ApiResponse,
    ):
        send_request.set_id(msg_id)

        response = req.post(
            f'{self.link}/send/{msg_id}',
            json=send_request.dict(),
            headers=self.headers)

        if response.status_code == 200:
            return Msg(**response.json())
        elif response.status_code == 404:
            raise NotFoundApi(response_status=response.status_code,
                              response_text=response.text)
        elif response.status_code == 400:
            raise BadRequestApi(response_status=response.status_code,
                                response_text=response.text)
        else:
            raise
