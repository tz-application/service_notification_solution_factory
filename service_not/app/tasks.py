import os
from datetime import timedelta

import django
from celery.utils.log import get_task_logger
from django.conf import settings
from django.utils import timezone

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_not.settings")
django.setup()
from app.models.message import StatusType
from app.models import Mailing, Client

from service.probe_fbrq import ProbeFbrq, ApiResponse
from service_not.celery import app

logger = get_task_logger(__name__)


@app.task
def check_work_run() -> dict:
    current_time = timezone.now()
    time_1m = current_time + timedelta(hours=1)
    mailings = Mailing.objects.filter(start_date__lt=current_time,
                                      end_date__gte=time_1m).prefetch_related('mess_mailing')
    for mailing in mailings:
        get_mailing.delay(mailing.id)

    if mailings.exists() is False:
        return {"cxz": "rew"}
    return {"fsdf": "trw"}


@app.task
def get_mailing(mailing_id: int) -> dict:
    mailing = Mailing.objects.filter(id=mailing_id).first()
    code_mobile = mailing.property_filter.get('code_mobile')
    tag = mailing.property_filter.get('tag')

    clients = Client.objects.filter(code_mobile=code_mobile, tag=tag)
    for client in iter(clients):
        check = mailing.mess_mailing.filter(client=client)
        if check.exists() is False:
            send_message.delay(client.id, mailing_id)

    return {"status": True, "mailing_id": mailing_id}


@app.task
def send_message(client_id: int, mailing_id: int, count: int = 3) -> dict:
    client = Client.objects.filter(id=client_id).first()
    mailing = Mailing.objects.filter(id=mailing_id).first()

    probe_fbrq = ProbeFbrq(settings.PROBE_FBRQ_TOKEN)
    try:
        send_mess = probe_fbrq.send_message(msg_id=123,
                                            send_request=ApiResponse(phone=client.phone, text=mailing.text))

        if send_mess.code == 0 or send_mess.message == "OK":
            mailing.mess_mailing.create(client=client, status=StatusType.SEND)
            return {"status": True, 'client_id': client_id, 'mailing_id': mailing_id}
    except:
        mailing.mess_mailing.create(client=client, status=StatusType.NO_SEND)

    if count == 0:
        return {"status": False, 'client_id': client_id, 'mailing_id': mailing_id}
    else:
        return send_message.delay(client.id, mailing_id, count - 1)


check_work_run()
