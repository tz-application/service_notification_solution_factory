import pytest
from django.conf import settings

from service.probe_fbrq import ProbeFbrq, ApiResponse


class TestService:
    def test_send_message(self, mocker):
        req_mock = mocker.patch.object(ProbeFbrq, 'send_message')
        req_mock.return_value.status_code = 200
        req_mock.return_value.code = 0
        req_mock.return_value.message = "fdgdsgdfg"

        probe_fbrq = ProbeFbrq(token=settings.PROBE_FBRQ_TOKEN)
        response = probe_fbrq.send_message(1, ApiResponse(text="tewdas", phone=79561234565))

        assert response.message == "fdgdsgdfg"
        assert response.code == 0
        assert response.status_code == 200

    def test_send_message_err(self, mocker):
        req_mock = mocker.patch.object(ProbeFbrq, 'send_message')
        req_mock.return_value.status_code = 400
        req_mock.return_value.code = 0
        req_mock.return_value.message = "string"

        probe_fbrq = ProbeFbrq(token=settings.PROBE_FBRQ_TOKEN)
        response = probe_fbrq.send_message(1, ApiResponse(text="tewdas", phone=79561234565, id=0))

        assert response.status_code == 400
        assert response.code == 0
        assert response.message == "string"

    def test_send_message_token(self, mocker):
        req_mock = mocker.patch.object(ProbeFbrq, 'send_message')
        req_mock.return_value.status_code = 401
        req_mock.return_value.code = -1
        req_mock.return_value.message = "token contains an invalid number of segments"

        probe_fbrq = ProbeFbrq(token="TOKEN")
        response = probe_fbrq.send_message(1, ApiResponse(text="tewdas", phone=79561234565))

        assert response.status_code == 401
        assert response.code == -1
        assert response.message == "token contains an invalid number of segments"
