import factory.django
from factory import fuzzy

from app.models import Message
from app.models.message import StatusType
from app.tests.factory.client_factory import ClientFactory
from app.tests.factory.mailing_factory import MailingFactory


class MessageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Message

    mailing = factory.SubFactory(MailingFactory)
    client = factory.SubFactory(ClientFactory)
    status = fuzzy.FuzzyChoice(StatusType)
