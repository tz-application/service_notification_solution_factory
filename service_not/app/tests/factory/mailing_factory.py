from datetime import timedelta

import factory.django
from django.utils import timezone
from factory import fuzzy

from app.models import Mailing

code_mobile = fuzzy.FuzzyInteger(100, 999)
tag = fuzzy.FuzzyText(length=7)


class MailingFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Mailing

    text = fuzzy.FuzzyText()
    property_filter = {"code_mobile": code_mobile.fuzz(), "tag": tag.fuzz()}
    start_date = timezone.now()
    end_date = timezone.now() + timedelta(minutes=2)
