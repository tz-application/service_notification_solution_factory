import factory.django
from factory import fuzzy

from app.models import Client

code_mobile = fuzzy.FuzzyInteger(100, 999)
number = fuzzy.FuzzyInteger(0000000, 9999999)
phone = f'7{code_mobile.fuzz()}{number.fuzz()}'

class ClientFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Client

    # id = factory.Sequence(lambda n: n)
    phone = int(phone)
    code_mobile = code_mobile
    tag = fuzzy.FuzzyText(length=7)
    time_zone = "Europe/Paris"
