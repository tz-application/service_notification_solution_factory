import pytest
from datatest import api_client, create_client


@pytest.mark.django_db
class TestClientViews:

    def test_client_create_g(self, api_client):
        response = api_client.post('/api/client/', {
            "phone": 79459999999,
            "code_mobile": 945,
            "tag": "string",
            "time_zone": "string"
        }, format='json')
        assert response.status_code == 201

        model = response.json()
        assert model.get('id') > 0
        assert model.get('phone') > 9999999999
        assert model.get('code_mobile') > 99
        assert model.get('code_mobile') < 1000

    def test_client_create_b(self, api_client):
        response = api_client.post('/api/client/', {
            "phone": 794599999999,
            "code_mobile": 9451,
            "tag": "",
            "time_zone": ""
        }, format='json')
        assert response.status_code == 400

    def test_client_partial_update_g(self, api_client, create_client):
        response = api_client.patch(f'/api/client/{create_client.id}/', {
            "phone": 79459999999,
            "code_mobile": 945,
            "tag": "string",
            "time_zone": "Europe/Paris"
        }, format="json")
        assert response.status_code == 200

        model = response.json()
        assert model.get('id') > 0
        assert model.get('phone') > 9999999999
        assert model.get('code_mobile') > 99
        assert model.get('code_mobile') < 1000
        assert str(model.get('code_mobile')) in str(model.get('phone'))

    def test_client_partial_update_b(self, api_client):
        response = api_client.patch(f'/api/client/0/', {
            "phone": 794599999999,
            "code_mobile": 200,
            "tag": "string",
            "time_zone": "Europe/Paris"
        }, format="json")
        assert response.status_code == 404

    def test_client_destroy_g(self, api_client, create_client):
        response = api_client.delete(f'/api/client/{create_client.id}/', format="json")
        assert response.status_code == 204

    def test_client_destroy_b(self, api_client):
        response = api_client.delete(f'/api/client/0/', format="json")
        assert response.status_code == 404
