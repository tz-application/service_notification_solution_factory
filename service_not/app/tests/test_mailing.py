import pytest

from app.models.message import StatusType
from datatest import api_client, create_mailings, create_mailing


@pytest.mark.django_db
class TestClientViews:
    def test_mailing_list_g(self, api_client, create_mailings):
        response = api_client.get('/api/mailing/')
        assert response.status_code == 200

        models = response.json()
        assert models.__len__() > 0

    def test_mailing_list_status_send_g(self, api_client, create_mailings):
        response = api_client.get('/api/mailing/', data={'status': StatusType.SEND})
        assert response.status_code == 200

        models = response.json()
        assert models.__len__() > 0

    def test_mailing_list_status_no_send_g(self, api_client, create_mailings):
        response = api_client.get('/api/mailing/', data={'status': StatusType.NO_SEND})
        assert response.status_code == 200

        models = response.json()
        assert models.__len__() > 0

    def test_mailing_retrieve_g(self, api_client, create_mailing):
        response = api_client.get(f'/api/mailing/{create_mailing.id}/')
        assert response.status_code == 200

        models = response.json()
        assert 'id' in models
        assert 'text' in models
        assert 'property_filter' in models
        assert 'code_mobile' in models['property_filter']
        assert 'tag' in models['property_filter']
        assert 'messages' in models
        assert 'message_send_count' in models
        assert 'start_date' in models
        assert 'end_date' in models

    def test_mailing_retrieve_b(self, api_client):
        response = api_client.get(f'/api/mailing/0/')
        assert response.status_code == 404

    def test_mailing_create_g(self, api_client):
        response = api_client.post(f'/api/mailing/', {
            "text": "string",
            "property_filter": {
                "code_mobile": 956,
                "tag": "string"
            },
            "start_date": "2022-12-05T15:59:06.503Z",
            "end_date": "2022-12-05T15:59:06.503Z"
        }, format='json')
        assert response.status_code == 201

        models = response.json()
        assert 'text' in models
        assert 'property_filter' in models
        assert 'code_mobile' in models['property_filter']
        assert 'tag' in models['property_filter']
        assert 'start_date' in models
        assert 'end_date' in models

    def test_mailing_create_b(self, api_client):
        response = api_client.post(f'/api/mailing/', json={
            "text": "string",
            "end_date": "2022-12-05T15:59:06.503Z"
        })
        assert response.status_code == 400

    def test_mailing_partial_update_g(self, api_client, create_mailing):
        response = api_client.patch(f'/api/mailing/{create_mailing.id}/', json={
            "text": "string",
            "property_filter": {
                "code_mobile": 956,
                "tag": "string"
            },
            "start_date": "2022-12-05T15:52:26.027Z",
            "end_date": "2022-12-05T15:52:26.027Z"
        })
        assert response.status_code == 200

    def test_mailing_partial_update_b(self, api_client):
        response = api_client.patch(f'/api/mailing/0/', json={
            "text": "",
            "property_filter": {"code_mobile": "853", "tag": ""},
            "start_date": "",
            "end_date": "2022-12-05T15:52:26.027Z"
        })
        assert response.status_code == 404

    def test_mailing_destroy_g(self, api_client, create_mailing):
        response = api_client.delete(f'/api/mailing/{create_mailing.id}/')
        assert response.status_code == 204

    def test_mailing_destroy_b(self, api_client):
        response = api_client.delete(f'/api/mailing/0/')
        assert response.status_code == 404
