import pytest

from app.models import Client, Mailing, Message
from app.tests.factory.client_factory import ClientFactory
from app.tests.factory.mailing_factory import MailingFactory
from app.tests.factory.message_factory import MessageFactory


@pytest.fixture
def api_client():
    """Возвращает APIClient для создания запросов."""
    from rest_framework.test import APIClient
    return APIClient()


@pytest.fixture
def create_client(db) -> Client:
    return ClientFactory()


@pytest.fixture
def create_mailing(db) -> Mailing:
    return MailingFactory.create()


@pytest.fixture
def create_mailings(db) -> Mailing:
    mailings = MailingFactory.create_batch(10)
    for mailing in mailings:
        for i in range(0, 5):
            mailing.mess_mailing.add(MessageFactory(mailing_id=mailing.id))
    return mailings


@pytest.fixture
def create_message(db) -> Message:
    return MessageFactory()


def mock_service(db):
    return
