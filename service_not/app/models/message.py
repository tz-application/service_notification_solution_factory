from django.db import models
from django.utils.translation import gettext_lazy as _


class StatusType(models.IntegerChoices):
    NO_SEND = 0, _('Не отправлен')
    SEND = 1, _('Отправлен')


# Create your models here.
class Message(models.Model):
    mailing = models.ForeignKey("app.Mailing", on_delete=models.CASCADE, verbose_name="Рассылка",
                                related_name="mess_mailing")
    client = models.ForeignKey("app.Client", on_delete=models.CASCADE, verbose_name="Клиент",
                               related_name="mess_client")
    status = models.IntegerField(verbose_name="Тип", choices=StatusType.choices, default=StatusType.NO_SEND)
    created_at = models.DateTimeField(verbose_name='Отправлено', auto_now_add=True)

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"

    def __str__(self):
        return f'{self.mailing} {self.client}'
