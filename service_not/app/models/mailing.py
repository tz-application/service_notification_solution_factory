from django.db import models


# Create your models here.
class Mailing(models.Model):
    text = models.TextField(verbose_name="Текст")
    property_filter = models.JSONField(verbose_name="Фильтр свойств",
                                       default={"code_mobile": "123", "tag": "string"})
    start_date = models.DateTimeField(verbose_name="Время запуска")
    end_date = models.DateTimeField(verbose_name="Время остановки")

    class Meta:
        verbose_name = "Рассылка"
        verbose_name_plural = "Рассылки"

    def __str__(self):
        return self.text