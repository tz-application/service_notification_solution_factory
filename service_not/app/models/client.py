from django.db import models


# Create your models here.
class Client(models.Model):
    phone = models.PositiveBigIntegerField(verbose_name="Номер телефона", default=74569999999)
    code_mobile = models.PositiveSmallIntegerField(verbose_name="Код мобильного оператора", default=100)
    tag = models.CharField(verbose_name="Тег", max_length=50)
    time_zone = models.CharField(verbose_name="Часовой пояс", max_length=25, help_text="Europe/Paris")

    class Meta:
        verbose_name = "Клиент"
        verbose_name_plural = "Клиенты"

    def __str__(self):
        return str(self.phone)
