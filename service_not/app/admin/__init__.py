from django.contrib import admin
from ..models import *


# Register your models here.
@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('phone', 'code_mobile', 'tag', 'time_zone')


@admin.register(Mailing)
class MailingAdmin(admin.ModelAdmin):
    list_display = ('text', 'property_filter', 'start_date', 'end_date')


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('mailing', 'client', 'status', 'created_at')
    list_filter = ('status',)
