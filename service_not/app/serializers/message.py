from rest_framework import serializers

from ..models import Message


class MessageListSerializer(serializers.ListSerializer):
    def to_representation(self, data):
        status = self.context.get('request').query_params.get('status')
        if status:
            data = data.filter(status=status)
        return super(MessageListSerializer, self).to_representation(data)


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        list_serializer_class = MessageListSerializer
        fields = '__all__'
