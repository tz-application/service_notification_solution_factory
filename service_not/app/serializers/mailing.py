from rest_framework import serializers

from ..models import Mailing
from .message import MessageSerializer
from .client import ClientSerializer
from ..models.message import StatusType


class PropertryFilterSerializer(serializers.Serializer):
    code_mobile = serializers.IntegerField(default=956, min_value=100, max_value=1000)
    tag = serializers.CharField(max_length=25)


class MailingSerializer(serializers.ModelSerializer):
    property_filter = PropertryFilterSerializer()
    messages = MessageSerializer(source='mess_mailing', many=True)
    message_send_count = serializers.SerializerMethodField()

    class Meta:
        model = Mailing
        fields = ('id',
                  'text',
                  'property_filter',
                  'messages',
                  'message_send_count',
                  'start_date',
                  'end_date',)

    def get_message_send_count(self, obj) -> int:
        return obj.mess_mailing.filter(status=StatusType.SEND).count()


class MailingEditSerializer(serializers.ModelSerializer):
    property_filter = PropertryFilterSerializer()

    class Meta:
        model = Mailing
        fields = ('id',
                  'text',
                  'property_filter',
                  'start_date',
                  'end_date',)
