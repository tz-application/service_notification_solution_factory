from rest_framework import serializers

from ..models import Client


class ClientSerializer(serializers.ModelSerializer):
    phone = serializers.IntegerField(default=79459999999, min_value=69999999999, max_value=80000000000,
                                     help_text="Номер телефона")
    code_mobile = serializers.IntegerField(default=945, min_value=100, max_value=1000,
                                           help_text="Код оператора")

    class Meta:
        model = Client
        fields = '__all__'
