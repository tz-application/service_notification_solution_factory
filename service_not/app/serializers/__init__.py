from .client import ClientSerializer
from .mailing import MailingSerializer, MailingEditSerializer
from .message import MessageSerializer
