# Generated by Django 4.1.3 on 2022-12-03 19:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Client",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("phone", models.PositiveIntegerField(verbose_name="Номер телефона")),
                (
                    "code_mobile",
                    models.SmallIntegerField(verbose_name="Код мобильного оператора"),
                ),
                ("tag", models.CharField(max_length=50, verbose_name="Тег")),
                (
                    "time_zone",
                    models.CharField(
                        help_text="Europe/Paris",
                        max_length=25,
                        verbose_name="Часовой пояс",
                    ),
                ),
            ],
            options={
                "verbose_name": "Клиент",
                "verbose_name_plural": "Клиенты",
            },
        ),
        migrations.CreateModel(
            name="Mailing",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("text", models.TextField(verbose_name="Текст")),
                (
                    "property_filter",
                    models.JSONField(
                        default={"code_mobile": "123", "tag": "string"},
                        verbose_name="Фильтр свойств",
                    ),
                ),
                ("start_date", models.DateTimeField(verbose_name="Время запуска")),
                ("end_date", models.DateTimeField(verbose_name="Время остановки")),
            ],
            options={
                "verbose_name": "Рассылка",
                "verbose_name_plural": "Рассылки",
            },
        ),
        migrations.CreateModel(
            name="Message",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "status",
                    models.IntegerField(
                        choices=[(0, "Отправлен"), (1, "Не отправлен")],
                        default=1,
                        verbose_name="Тип",
                    ),
                ),
                (
                    "created_at",
                    models.DateTimeField(auto_now_add=True, verbose_name="Отправлено"),
                ),
                (
                    "client",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="mess_client",
                        to="app.client",
                        verbose_name="Клиент",
                    ),
                ),
                (
                    "mailing",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="mess_mailing",
                        to="app.mailing",
                        verbose_name="Рассылка",
                    ),
                ),
            ],
            options={
                "verbose_name": "Сообщение",
                "verbose_name_plural": "Сообщения",
            },
        ),
    ]
