from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, viewsets, mixins, authentication, permissions
from ..models import *
from ..serializers import *


class ClientViewset(
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet
):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    permission_classes = [permissions.AllowAny]

    @swagger_auto_schema(responses={
        201: openapi.Response("Объект клиента", ClientSerializer)
    }, operation_summary="Добавление клиента")
    def create(self, request, *args, **kwargs):
        return super(ClientViewset, self).create(request, *args, **kwargs)

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter('id',
                          openapi.IN_QUERY,
                          required=True,
                          description="ID объекта",
                          type=openapi.TYPE_INTEGER)
    ], responses={
        200: openapi.Response("Объект клиента", ClientSerializer)
    }, operation_summary="Обновление данных клиента")
    def partial_update(self, request, *args, **kwargs):
        return super(ClientViewset, self).partial_update(request, *args, **kwargs)

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter('id',
                          openapi.IN_QUERY,
                          required=True,
                          description="ID объекта",
                          type=openapi.TYPE_INTEGER)
    ], operation_summary="Удаление клиента")
    def destroy(self, request, *args, **kwargs):
        return super(ClientViewset, self).destroy(request, *args, **kwargs)

    http_method_names = ['post', 'patch', 'delete']
