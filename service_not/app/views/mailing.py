from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, viewsets, mixins, authentication, permissions

from ..mixins import MultiSerializerViewSetMixin
from ..models import *
from ..models.message import StatusType
from ..serializers import *


class MailingAPIView(
    MultiSerializerViewSetMixin,
    viewsets.ModelViewSet
):
    queryset = Mailing.objects.all()
    serializer_action_classes = {
        'list': MailingSerializer,
        'retrieve': MailingSerializer,
        'create': MailingEditSerializer,
        'update': MailingEditSerializer,
        'partial_update': MailingEditSerializer,
    }
    http_method_names = ['get', 'post', 'patch', 'delete']
    permission_classes = [permissions.AllowAny]

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter('status',
                          openapi.IN_QUERY,
                          required=False,
                          description="Статус сообщения",
                          type=openapi.TYPE_INTEGER,
                          enum=[status.value for status in StatusType])
    ], responses={200: openapi.Response('Объект рассылки', MailingSerializer(many=True))},
        operation_summary="Все рассылки")
    def list(self, request, *args, **kwargs):
        status = request.query_params.get('status')
        if status:
            self.queryset = Mailing.objects.filter(mess_mailing__status=status).distinct()
        return super().list(request, *args, **kwargs)

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter('id',
                          openapi.IN_QUERY,
                          required=True,
                          description="ID объекта",
                          type=openapi.TYPE_INTEGER)
    ], responses={200: openapi.Response('Объект рассылки', MailingSerializer)},
        operation_summary="Получить рассылку по id")
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)

    @swagger_auto_schema(responses={
        201: openapi.Response('Объект рассылки', MailingEditSerializer)
    }, request_body=MailingEditSerializer,
        operation_summary="Добавление рассылки")
    def create(self, request, *args, **kwargs):
        return super(MailingAPIView, self).create(request, *args, **kwargs)

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter('id',
                          openapi.IN_QUERY,
                          required=True,
                          description="ID объекта",
                          type=openapi.TYPE_INTEGER)
    ], responses={
        200: openapi.Response('Объект рассылки', MailingEditSerializer)
    }, request_body=MailingEditSerializer,
        operation_summary="Обновление данных рассылки")
    def partial_update(self, request, *args, **kwargs):
        return super(MailingAPIView, self).partial_update(request, *args, **kwargs)

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter('id',
                          openapi.IN_QUERY,
                          required=True,
                          description="ID объекта",
                          type=openapi.TYPE_INTEGER)
    ], operation_summary="Удалить рассылку")
    def destroy(self, request, *args, **kwargs):
        return super(MailingAPIView, self).destroy(request, *args, **kwargs)
