# TZ - API

## Запустить

1. Склонировать проект на пк: `git clone https://gitlab.com/tz-application/service_notification_solution_factory.git`
2. Перейти в папку с проектом: `cd service_notification_solution_factory`
3. Изменить файл `.env.example` на `.env`: `mv .env.example .env`
4. Указать ключ в `.env` файле в строке `PROBE_FBRQ_TOKEN`
5. Сбилдить и запустить проект: `docker-compose up --build`

## Что можно улучшить

Это не реализовывал, так как небольшой функционал, не особо есть смысл, но это очень желательно если в дальнейшем
развивать проект. Если бы я проектировал бы большой апи, я бы придерживался бы этой логики.

1. Вынести работу с сервисом/сервисами в отдельный модуль. Зачем? Декомпозиция.
2. Для апи реализовать version, по типу `api/v1`,`api/v2`,`api/v3`. Зачем? Допустим у нас сервис развивается, апи
   растет, люди им пользуются, функционал растет или же мы не учли изначально какой-либо функционал. Да, изменять старое
   апи можно, но если его кто-то использует, то может отпасть много разных вещей у тех кто использует его. Если
   изначально мы спроектировали version для апи, то нам ничего не мешает создать новую версию api/v2 и перейти на нее
3. Добавить маску для мобильного номера телефона. Так как сейчас сделано по ограничению в длине числа. Больше
   69999999999 и меньше 80000000000

## Дополнительные задания - Сделано

|Задание|Сделано|Инфо|
|:---:|:---:|:---:|
|1|+|-|
|2|-|-|
|3|+|-|
|4|-|-|
|5|+|-|
|6|-|-|
|7|-|-|
|8|-|-|
|9|+-|если возникли ошибки, пытается 3 раза отправить|
|10|-|-|
|11|-|-|
|12|-|-|

### Задания

1. организовать тестирование написанного кода
2. обеспечить автоматическую сборку/тестирование с помощью GitLab CI
3. подготовить docker-compose для запуска всех сервисов проекта одной командой
4. написать конфигурационные файлы (deployment, ingress, …) для запуска проекта в kubernetes и описать как их применить
   к работающему кластеру
5. сделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI и в нём отображалось описание разработанного
   API. Пример: https://petstore.swagger.io
6. реализовать администраторский Web UI для управления рассылками и получения статистики по отправленным сообщениям
7. обеспечить интеграцию с внешним OAuth2 сервисом авторизации для административного интерфейса.
   Пример: https://auth0.com
8. реализовать дополнительный сервис, который раз в сутки отправляет статистику по обработанным рассылкам на email
9. удаленный сервис может быть недоступен, долго отвечать на запросы или выдавать некорректные ответы. Необходимо
   организовать обработку ошибок и откладывание запросов при неуспехе для последующей повторной отправки. Задержки в
   работе внешнего сервиса никак не должны оказывать влияние на работу сервиса рассылок.
10. реализовать отдачу метрик в формате prometheus и задокументировать эндпоинты и экспортируемые метрики
11. реализовать дополнительную бизнес-логику: добавить в сущность "рассылка" поле "временной интервал", в котором можно
    задать промежуток времени, в котором клиентам можно отправлять сообщения с учётом их локального времени. Не
    отправлять клиенту сообщение, если его локальное время не входит в указанный интервал.
12. обеспечить подробное логирование на всех этапах обработки запросов, чтобы при эксплуатации была возможность найти в
    логах всю информацию по
    1. id рассылки - все логи по конкретной рассылке (и запросы на api и внешние запросы на отправку конкретных
       сообщений)
    2. id сообщения - по конкретному сообщению (все запросы и ответы от внешнего сервиса, вся обработка конкретного
       сообщения)
    3. id клиента - любые операции, которые связаны с конкретным клиентом (добавление/редактирование/отправка
       сообщения/…)

## Документация по API для интеграции с разработанным сервисом

Интеграция с моим сервисом

### API сервиса

Документация: `http://localhost:8000/docs/`

Ссылка для запросов: `http://localhost:8000/api`

#### Клиент

|Ссылка|Метод|Информация|
|:---:|:---:|:---|
|/client/|post|добавление нового клиента|
|/client/:id/|patch|обновление клиента|
|/client/:id/|delete|удаление клиента|

#### Рассылка

|Ссылка|Метод|Информация|
|:---:|:---:|:---|
|/mailing/|get|получение списка рассылок|
|/mailing/:id/|get|получить информацию о рассылке|
|/mailing/|post|добавление новой рассылки|
|/mailing/:id/|patch|обновление рассылки|
|/mailing/:id/|delete|удаление рассылки|

## Интеграция с сервисом [probe.fbrq.cloud](https://probe.fbrq.cloud/docs#/send/sendMsg)

### Отправка сообщения

Метод для отправки сообщений.

```python
probe = ProbeFbrq(token="TOKEN")
message_info = probe.send_message(31, ApiResponse(text="text", phone=79999999999))
```